name             'gm_hadoop_accessConfig'
license          'All rights reserved'
description      'Installs/Configures gm_hadoop_accessConfig'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
maintainer       'General Motors'
maintainer_email 'rick.connelly@gm.com'
chef_version '>= 12'
issues_url 'https://gm-com.socialcast.com/groups/186552-chef' if respond_to?(:issues_url)
source_url 'https://bitbucket.gm.com/projects/HAD125874'

depends 'gm_access_config'
