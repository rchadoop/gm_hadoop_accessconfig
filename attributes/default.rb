default['sys']['custom']['access_default'] = 'true'
default['sys']['custom']['access'] = [
'# ',
'# BELOW ADDED BY THE gm_hadoop_accessConfig COOKBOOK',
'+ : (hdpsa) : ALL',
'+ : (ambops) : ALL',
'# END OF gm_hadoop_accessConfig COOKBOOK ENTRIES'
]
