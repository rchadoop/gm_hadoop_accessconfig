# gm_hadoop_accessConfig Cookbook

-  This cookbook configures the /etc/security/access.conf, with the values in the gm_hadoop_accessConfig/attributes directory, 
-   This wrapper cookbook uses cookbook "gm_access_config as a base cookbook, and adds entries specific to the hadoop application.

## Requirements

- Valid chef environment

### Platforms

- OEL 6.9 (Probably is not specific that just that, but only tested here)

### Chef

- Chef 12.0 or later

### Cookbooks

- No dependencies on other cookbooks

## Attributes
- At present, the base attributes are given below ...

   \+ : (hdpsa) : ALL

   \+ : (ambops) : ALL'


## Usage

This cookbook is comprised of 1 recipe, and one attributes file:   

   *  default.rb    
      This recipe calls the base cookbook "gm_access_config".
   
   *  attributes/default.rb
      This attributes file is where you would enter hadoop specific entries.
   
   
     
## License and Authors

Authors: Rick Connelly & James Mullaney

