# # encoding: utf-8

# Inspec test for recipe gm_hadoop_gdss_prodm::default

control 'Access_conf-1.1' do
  impact 0.3
  title 'Verify Root Access in access.conf'
  desc 'Checking Root Allowed access from Management Server in access.conf'
  describe file("/etc/security/access.conf") do
    its("content") { should match /\+ : \(hdpsa\) : ALL/ }
  end
end

control 'Access_conf-1.2' do
  impact 0.3
  title 'Verify Root Access in access.conf'
  desc 'Checking Root Allowed access from Management Server in access.conf'
  describe file("/etc/security/access.conf") do
    its("content") { should match /\+ : \(ambops\) : ALL/ }
  end
end

